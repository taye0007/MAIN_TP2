using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CamChanger : MonoBehaviour
{
    public List<Camera> Cameras;

    private void Start()
    {
        EnableCamera(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            EnableCamera(0);
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            EnableCamera(1);
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            EnableCamera(2);
        }


    }

    private void EnableCamera(int n)
    {
        Cameras.ForEach(cam => cam.enabled = false);
        Cameras.ForEach(cam => cam.gameObject.SetActive(false));
        Cameras[n].gameObject.SetActive(true);
        Cameras[n].enabled = true;
    }
}